// use dynamodb aws sdk to interact with db
use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use rand::Rng;
use serde::{Deserialize, Serialize};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

// data to push to the dynamodb table
#[derive(Debug, Serialize, Deserialize)]
pub struct SongOfTheDay {
    pub day_id: String,
    pub song_value: String,
}
// required for AWS
#[derive(Debug, Serialize)]
struct FailureResponse {
    pub body: String,
}

// implement Display for the Failure response so that we can then implement Error.
impl std::fmt::Display for FailureResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.body)
    }
}
// this will give the incoming request to add or view results
// command is either "add" or "view"
// implement add first then view
#[derive(Deserialize)]
struct Request {
    command: String,
}

// the response will show what the person flipped or view last 10 flips
#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

// main body function to add or view flips
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let r_id = event.context.request_id;
    // focus on adding flips first
    let mut rng = rand::thread_rng();
    let song_number: u8 = rng.gen_range(0..=1);
    let result = if song_number == 0 {
        "Sunshine"
    } else {
        "Flowers"
    };
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);
    // plug in flip to request struct
    let mut song_of_the_day = SongOfTheDay {
        day_id: String::new(),
        song_value: String::new(),
    };
    // set value
    song_of_the_day.day_id = String::from(r_id.clone());
    song_of_the_day.song_value = String::from(result);
    // prepare to store value
    let song_of_the_day_id = AttributeValue::S(song_of_the_day.day_id.clone());
    let song_of_the_day_value = AttributeValue::S(song_of_the_day.song_value.to_string());
    // add to dynamodb
    // store our data in the coinflip table
    let _resp = client
        .put_item()
        .table_name("SongOfTheDay")
        .item("day_id", song_of_the_day_id)
        .item("song_value", song_of_the_day_value)
        .send()
        .await
        .map_err(|_err| FailureResponse {
            body: _err.to_string(),
        });

    // prepare the response
    let resp = Response {
        req_id: r_id,
        msg: format!("Your song of the day {:?}.", result),
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}

#[cfg(test)]
mod tests {
    use crate::{function_handler, Request};
    use lambda_runtime::{Context, LambdaEvent};

    #[tokio::test]
    async fn response_is_good_for_simple_input() {
        let id = "123456789";

        let mut context = Context::default();
        context.request_id = id.to_string();

        let payload = Request {
            command: "X".to_string(),
        };
        let event = LambdaEvent { payload, context };

        let result = function_handler(event).await.unwrap();

        assert_eq!(result.req_id, id.to_string());
    }
}
