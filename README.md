Markdown
[![pipeline status](https://gitlab.com/xw263/mini_project_5/badges/main/pipeline.svg)](https://gitlab.com/xw263/mini_project_5/-/commits/main)
HTML
<a href="https://gitlab.com/xw263/mini_project_5/-/commits/main"><img alt="pipeline status" src="https://gitlab.com/xw263/mini_project_5/badges/main/pipeline.svg" /></a>
AsciiDoc
image:https://gitlab.com/xw263/mini_project_5/badges/main/pipeline.svg[link="https://gitlab.com/xw263/mini_project_5/-/commits/main",title="pipeline status"]

# mini_project_5

## Purpose
The goal of this project is to create a serverless AWS Lambda function using the Rust programming language and Cargo Lambda and use a database such as AWS DynamoDB. This project created a lambda function that randomly generate a song for the user, it also has a Dynamo DB to record the result of each generation.

## Microservice Link
https://bb7zygoqz7.execute-api.us-east-1.amazonaws.com/initial_deploy/mini5

## Dynamo DB Table
![Screenshot_2024-02-25_at_5.51.30_PM](/uploads/218bab04e44ae19c5d93ae518c785039/Screenshot_2024-02-25_at_5.51.30_PM.png)

## Test Results
![Screenshot_2024-02-25_at_5.52.29_PM](/uploads/753482e85dde660edefc641f924cf53a/Screenshot_2024-02-25_at_5.52.29_PM.png)

## Steps
1. Services Required
    1. Rust (`brew install rust`)
    2. Cargo Lambda  `brew tap cargo-lambda/cargo-lambda`, `brew install cargo-lambda`
    3. AWS account (Sign up for the free tier)
2. Write the function locally
    1. Set up your Cargo Lambda project `cargo lambda new new-lambda-project`.
    2. Modify / write the template in main.rs to achieve the funtion you want to have.
    3. Do `cargo lambda watch` to test locally.
    4. Do `cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }" `to test the Lambda function (arguments passed in may be different).
3. Set up AWS IAM User
    1. Go to the AWS IAM web management console and add an IAM User for credentials.
    2. Attach policies lambdafullaccess and iamfullaccess.
    3. Finish user creation, open user details, and go to security credentials.
    4. Generate access key.
4. Connect AWS with local function
    1. Store AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (your choice) in a .env file that won't be pushed with your repo (add to .gitignore).
    2. Export the vars by using `set -a # automatically export all variables                       
    source .env
    set +a` in the terminal. This lets cargo lambda know which AWS account to deploy to.
    3.  add AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (your choice) to gitlab secrets
    4. Use my .gitlab-ci.yml to enable auto build, test, and deploy of your lambda function every time you push your repo
    5. Push the files from local to Gitlab
    6. Test it in the AWS lambda function
5. Add lambda function and Dynamo DB
    1. Login to AWS Lambda (make sure you have the region correct on the top right bar) and check you have it installed.
    2. Add permission to let your lambda function access DynamoDB by going to configuration in your lambda function page and click the arn name (role name) of your function add policies.
    3. Add permission by adding the AWSDynamoDBFullAccess to the function permissions.
    4. Add a DynamoDB table by going to the AWS console again to create a table and setting a primary key (make sure this key is a field you are using when you are interacting with your lambda function eg. "day_id" is my primary key).
6. Connect the Lambda function with AWS API Gateway
    1. Create a new API (keep default settings, `REST API`) then create a new resource (the URL path that will be appended to your API link).
    2. Select the resource, add method type `ANY` and select Lambda function.
    3. Turn off Lambda proxy integration if it's on.
    4. Deploy your API by adding a new stage (another string that will be appended to your URL path).
    5. After clicking deploy, find stages and find your invoke URL.

## Reference:
https://gitlab.com/jeremymtan/jeremytan_ids721_week5


